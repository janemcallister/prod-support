## Starting Point for updating petclinic
## Assuming new version has been copied to /home/grads/petclinic_update.tar.gz

# scp petclinic_v2.tar.gz grads@appgradsbf.eu.conygre.com:pet_clinic_update.tar.gz

## 1. Stop the service
sudo service petclinic stop
# TODO: make sure it's stopped e.g. ps afx | grep petclinic

## 2. Rotate the directories
sudo mv /opt/petclinic /opt/petclinic_v1
sudo tar zxvf petclinic_update.tar.gz -C /opt/

## 3. Database backup
mysqldump -h 172.31.5.244 -u petclinic -pC0nygre@ petclinic > petclinic_v1.sql

# ?do we have a database migration script? if so, migrate now

# 4. Restart the service at the new version
sudo service petclinic start
# TODO: test to make sure it's up - e.g. curl
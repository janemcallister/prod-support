## Starting Point for updating petclinic
## Assuming new version has been copied to /home/grads/petclinic_update.tar.gz

# scp petclinic_v2.tar.gz grads@appgradsbf.eu.conygre.com:pet_clinic_update.tar.gz

## 1. Stop the service
sudo service moodle stop
# TODO: make sure it's stopped e.g. ps afx | grep petclinic

## 3. Database backup
mysqldump -h 172.31.5.244 -u moodle -pC0nygre moodle > petclinic_v1.sql

# ?do we have a database migration script? if so, migrate now

# 4. Restart the service at the new version
sudo service moodle start
# TODO: test to make sure it's up - e.g. curl
# 1. Stop the running httpd/apache service
sudo service httpd stop
sleep 10

# 2. Create a backup directory and Backup the config file
sudo rm -fr /tmp/moodle_39_backup
mkdir /tmp/moodle_39_backup
cd /var/www/html
sudo cp config.php /tmp/moodle_39_backup

# 3. Backup the database
mysqldump -h 172.31.5.244 -p3316 -u moodle -pc0nygre moodle > /tmp/moodle_39_backup/moodle_39_backup.sql

# 4. Switch to the new fileset
sudo git checkout MOODLE_38_STABLE

# 5. Update the config file to use the v39 database
sudo sed -i 's/172.31.5.244:3316/172.31.5.244:3306/' config.php

# 6. Restart the service and test with curl
sudo service httpd start
sleep 10

if curl http://appgradsbf.eu.conygre.com/ | grep -q Test;
then
    echo 'Service has restarted successfully'
else
    echo 'Test of moodle homepage failed'
fi

# TODO: 7. Send the backup to aws
# aws cp /tmp/moodle_38_backup
# rm -fr /tmp/moodle_38_backup
